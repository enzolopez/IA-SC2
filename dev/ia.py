from sc2 import run_game, maps, Race, Difficulty
from sc2.player import Bot, Computer
from Bots import *

while True:
    run_game(maps.get('(2)RedshiftLE'), [
        Bot(Race.Zerg, AlekZergBot()),
        Computer(Race.Protoss, Difficulty.Easy)
    ], realtime=False)
